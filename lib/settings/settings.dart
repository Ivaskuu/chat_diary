import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Settings extends StatefulWidget
{
  @override
  State createState() => new SettingsState();
}

class SettingsState extends State<Settings>
{
  SharedPreferences prefs;

  bool enablePIN = false;
  int pinType = 0;
  RefVar onAppLaunchPIN = new RefVar(false);
  RefVar onDailyChatOpenPIN = new RefVar(false);
  RefVar onDailyChatEditPIN = new RefVar(false);

  @override
  void initState()
  {
    super.initState();
    getPinPrefs().then((_) => setState((){}));
  }

  getPinPrefs() async
  {
    prefs = await SharedPreferences.getInstance();
    if(prefs.getBool("enablePin") != null)
    {
      enablePIN = prefs.getBool("enablePin");
      pinType = prefs.getInt("pinType");
      onAppLaunchPIN.val = prefs.getBool("onAppLaunchPin");
      onDailyChatOpenPIN.val = prefs.getBool("onDailyChatOpenPin");
      onDailyChatEditPIN.val = prefs.getBool("onDailyChatEditPin");
    }
    else
    {
      prefs.setBool("enablePin", false);
      prefs.setInt("pinType", 0);
      prefs.setBool("onAppLaunchPin", false);
      prefs.setBool("onDailyChatOpenPin", false);
      prefs.setBool("onDailyChatEditPin", false);

      enablePIN = false;
      pinType = null;
      onAppLaunchPIN.val = false;
      onDailyChatOpenPIN.val = false;
      onDailyChatEditPIN.val = false;

      prefs.commit();
    }
  }

  @override
  Widget build(BuildContext context)
  {
    return new Scaffold
    (
      appBar: new AppBar
      (
        backgroundColor: Colors.white,
        elevation: 2.5,
        leading: new IconButton(icon: new Icon(Icons.arrow_back), color: Colors.black, onPressed: () => Navigator.of(context).pop()),
        title: new Text("Settings", style: new TextStyle(color: Colors.black))
      ),
      body: new ListView
      (
        children: <Widget>
        [
          categoryText("Privacy"),
          new SwitchListTile
          (
            value: enablePIN,
            title: new Text("Enale PIN protection"),
            onChanged: (newValue) => setState(()
            {
              newValue == true
              ? Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new ConfigurePIN())).then
              (
                (int newPinType)
                {
                  if(newPinType != null)
                  {
                    pinType = newPinType;
                    prefs.setInt("pinType", newPinType);
                    
                    enablePIN = true;
                    prefs.setBool("enablePin", true);

                    onAppLaunchPIN.val = true;
                    prefs.setBool("onAppLaunchPin", true);
                  }
                }
              )
              : setState(() { enablePIN = false; prefs.setBool("enablePin", false); });}
            )
          ),
          new ListTile
          (
            enabled: enablePIN,
            title: new Text("Set PIN type"),
            subtitle: new Text(pinTypeToString()),
            onTap: () => Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new ConfigurePIN())).then
            (
              (int newPinType)
              {
                if(newPinType != null)
                {
                  setState(() => pinType = newPinType);
                  prefs.setInt("pinType", newPinType);
                }
              }
            )
          ),
          customCheckboxTile
          (
            "Ask for PIN on app launch",
            "The DailyChats will be hidden from intruders",
            onAppLaunchPIN,
            enablePIN,
            varName: "onAppLaunchPin"
          ),
          customCheckboxTile
          (
            "Ask for PIN on DailyChat open",
            "The chats will be hidden from intruders",
            onDailyChatOpenPIN,
            enablePIN,
            varName: "onDailyChatOpenPin"
          ),
          customCheckboxTile
          (
            "Ask for PIN on DailyChat edit",
            "You will need to insert the PIN to edit DailyChats",
            onDailyChatEditPIN,
            enablePIN,
            varName: "onDailyChatEditPin"
          ),
          new Divider(),
          categoryText("Security"),
        ]
      )
    );
  }

  Widget categoryText(String cat)
  {
    return new Container(margin: new EdgeInsets.fromLTRB(16.0, 10.0, 0.0, 2.0), child: new Text(cat, textScaleFactor: 1.1, style: new TextStyle(color: Colors.blue, fontWeight: FontWeight.w700)));
  }

  Widget customCheckboxTile(String title, String subtitle, RefVar valueVar, bool enabled, {String varName})
  {
    return new CheckboxListTile
    (
      value: valueVar.val,
      title: new Text(title),
      subtitle: new Text(subtitle),
      onChanged: enabled ? (bool newValue) { setState(() => valueVar.val = newValue); varName != null ? prefs.setBool(varName, newValue) : null; } : null
    );
  }

  saveToPrefs(String varName, var varValue)
  {
    if(varName.runtimeType == bool)
    {
      prefs.setBool(varName, varValue);
    }
    else if(varName.runtimeType == int)
    {
      prefs.setInt(varName, varValue);
    }
    else if(varName.runtimeType == bool)
    {
      prefs.setString(varName, varValue);
    }
  }

  String pinTypeToString()
  {
    switch(pinType){ case 0: return "Numbers"; break; case 1: return "Password"; break; case 2: return "Words"; break; case 3: return "Taps"; break; default: return "No type selected"; break;}
  }
}

class RefVar
{
  var val;
  RefVar(this.val);
}

class ConfigurePIN extends StatefulWidget
{
  @override
  State createState() => new ConfigurePINState();
}

String pinAppBarTitle = "Choose a PIN";

class ConfigurePINState extends State<ConfigurePIN>
{
  int typeOfPin;
  PageController pageController = new PageController();
  bool showNewPinPage = false;
  bool showTestPinPage = false;

  @override
  Widget build(BuildContext context)
  {
    return new Scaffold
    (
      appBar: new AppBar
      (
        leading: new IconButton(icon: new Icon(Icons.close), onPressed: () => Navigator.of(context).pop()),
        title: new Text(pinAppBarTitle),
        backgroundColor: Colors.black
      ),
      body: pageView()
    );
  }

  Widget pageView()
  {
    if(!showNewPinPage)
    {
      setState(() => pinAppBarTitle = "Choose a PIN");
      return new PageView
      (
        controller: pageController,
        scrollDirection: Axis.horizontal,
        children: <Widget>
        [
          pinTypePage()
        ]
      );
    }
    else if(!showTestPinPage)
    {
      setState(() => pinAppBarTitle = "Set a new PIN");
      return new PageView
      (
        controller: pageController,
        scrollDirection: Axis.horizontal,
        children: <Widget>
        [
          pinTypePage(),
          newPinPage()
        ]
      );
    }
    else
    {
      setState(() => pinAppBarTitle = "Test the new PIN");
      return new PageView
      (
        controller: pageController,
        scrollDirection: Axis.horizontal,
        children: <Widget>
        [
          pinTypePage(),
          newPinPage(),
          testPinPage()
        ]
      );
    }
  }

  Widget pinTypePage()
  {
    return new ListView
    (
      children: <Widget>
      [
        new ListTile
        (
          title: new Text("Numbers"),
          subtitle: new Text("A 4 digits code"),
          onTap: () {setState(() { typeOfPin = 0; showNewPinPage = true; }); gotoNextPage();}
        ),
        new ListTile
        (
          title: new Text("String"),
          subtitle: new Text("A variable length string composed of alphabetical characters"),
          onTap: () {setState(() { typeOfPin = 1; showNewPinPage = true; }); gotoNextPage();}
        ),
        new ListTile
        (
          title: new Text("Words"),
          subtitle: new Text("A sequence of words"),
          onTap: () {setState(() { typeOfPin = 2; showNewPinPage = true; }); gotoNextPage();}
        ),
        new ListTile
        (
          title: new Text("Taps"),
          subtitle: new Text("A sequence of taps"),
          onTap: () {setState(() { typeOfPin = 3; showNewPinPage = true; }); gotoNextPage();}
        )
      ]
    );
  }

  String code;
  List<Widget> words = new List<Widget>();
  List<int> knocks = new List<int>();
  
  Widget newPinPage()
  {
    switch(typeOfPin)
    {
      case 0:
        TextEditingController controller = new TextEditingController();
        return new Container
        (
          margin: new EdgeInsets.all(10.0),
          child: new Column
          (
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>
            [
              new Text("Write a 4 digits code that you will remember. Evit using a repeating pattern (like '0000') or an easy to guess code (like '1234' or your birth date '1987')"),
              new TextField
              (
                controller: controller,
                keyboardType: TextInputType.number,
                onSubmitted: (String choosenCode) { code = choosenCode; showTestPinPage = true; gotoNextPage(); }
              ),
              new RaisedButton(child: new Text("CONTINUE"), onPressed: () { code = controller.text; showTestPinPage = true; gotoNextPage(); })
            ]
          )
        );
        break;
      case 1:
        TextEditingController controller = new TextEditingController();
        return new Container
        (
          margin: new EdgeInsets.all(10.0),
          child: new Column
          (
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>
            [
              new Text("Write a characters sequence that you will remeber."),
              new TextField
              (
                controller: controller,
                keyboardType: TextInputType.text,
                onSubmitted: (String choosenCode) { code = choosenCode; showTestPinPage = true; gotoNextPage(); }
              ),
              new RaisedButton(child: new Text("CONTINUE"), onPressed: () { code = controller.text; showTestPinPage = true; gotoNextPage(); })
            ]
          )
        );
        break;
      case 2:
        TextEditingController controller = new TextEditingController();

        return new Container
        (
          margin: new EdgeInsets.all(16.0),
          child: new Column
          (
            children: <Widget>
            [
              new Container(margin: new EdgeInsets.only(bottom: 12.0), child: new Text("Your words :", style: Theme.of(context).textTheme.title)),
              words.length > 0 ? new Wrap
              (
                spacing: 8.0,
                runSpacing: 4.0,
                children: words,
              ) : new Text("Add a word by clicking on the ADD button"),
              new Divider(),
              new Row
              (
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>
                [
                  new Expanded
                  (
                    child: new TextField
                    (
                      controller: controller,
                      keyboardType: TextInputType.text,
                      onSubmitted: (String word) {setState(() => words.add(new Chip(label: new Text(word))));}
                    )
                  ),
                  new Container(margin: new EdgeInsets.only(left: 8.0), child: new RaisedButton(child: new Text("ADD"), onPressed: () => setState(() => words.add(new Chip(label: new Text(controller.text))))))
                ]
              ),
              new RaisedButton(child: new Text("CONTINUE"), onPressed: () { showTestPinPage = true; gotoNextPage(); })
            ]
          )
        );
        break;
      default:
        return new Container
        (
          margin: new EdgeInsets.all(10.0),
          child: new Column
          (
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>
            [
              new Container(margin: new EdgeInsets.symmetric(vertical: 24.0, horizontal: 8.0), child: new Text("Instructions: Create a tap sequence by tapping on the 4 squares (the squares will not be coloured in the future).", textAlign: TextAlign.center)),
              new Container(margin: new EdgeInsets.only(bottom: 34.0), child: new Text("Taps num : ${knocks.length}", style: Theme.of(context).textTheme.title)),
              new Expanded(child:new GridView.count
              (
                crossAxisCount: 2,
                children: <Widget>
                [
                  new Container(color: Colors.red, child: new InkWell(onTap: () => setState(() => knocks.add(0)))),
                  new Container(color: Colors.blue, child: new InkWell(onTap: () => setState(() => knocks.add(1)))),
                  new Container(color: Colors.orange, child: new InkWell(onTap: () => setState(() => knocks.add(2)))),
                  new Container(color: Colors.teal, child: new InkWell(onTap: () => setState(() => knocks.add(3))))
                ]
              )),
              new RaisedButton(child: new Text("CONTINUE"), onPressed: () { setState(() => showTestPinPage = true); gotoNextPage(); })
            ]
          )
        );
        break;
    }
  }
  
  List<int> tapsTest = new List<int>();

  Widget testPinPage()
  {
    switch(typeOfPin)
    {
      case 0:
        TextEditingController controller = new TextEditingController();
        return new Container
        (
          margin: new EdgeInsets.all(10.0),
          child: new Column
          (
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>
            [
              new Text("Enter your new code"),
              new TextField
              (
                controller: controller,
                keyboardType: TextInputType.number,
                onChanged: (_) {if(controller.text == code) {savePinAndPop();}}
              )
            ]
          )
        );
        break;
      case 1:
        TextEditingController controller = new TextEditingController();
        return new Container
        (
          margin: new EdgeInsets.all(10.0),
          child: new Column
          (
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>
            [
              new Text("Enter your new code"),
              new TextField
              (
                controller: controller,
                keyboardType: TextInputType.text,
                onChanged: (_) {if(controller.text == code) {savePinAndPop();}}
              )
            ]
          )
        );
        break;
      case 2:
        TextEditingController controller = new TextEditingController();

        return new Container
        (
          margin: new EdgeInsets.all(16.0),
          child: new Column
          (
            children: <Widget>
            [
              new Container(margin: new EdgeInsets.only(bottom: 12.0), child: new Text("Your words :", style: Theme.of(context).textTheme.title)),
              words.length > 0 ? new Wrap
              (
                spacing: 8.0,
                runSpacing: 4.0,
                children: words,
              ) : new Text("Add a word by clicking on the ADD button"),
              new Divider(),
              new Row
              (
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>
                [
                  new Expanded
                  (
                    child: new TextField
                    (
                      controller: controller,
                      keyboardType: TextInputType.text,
                      onSubmitted: (String word) {setState(() => words.add(new Chip(label: new Text(word))));}
                    )
                  ),
                  new Container(margin: new EdgeInsets.only(left: 8.0), child: new RaisedButton(child: new Text("ADD"), onPressed: () => setState(() => words.add(new Chip(label: new Text(controller.text))))))
                ]
              ),
              new RaisedButton(child: new Text("CONTINUE"), onPressed: () => gotoNextPage())
            ]
          )
        );
        break;
      default:
        return new Container
        (
          margin: new EdgeInsets.all(10.0),
          child: new Column
          (
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>
            [
              new Container(margin: new EdgeInsets.symmetric(vertical: 64.0, horizontal: 8.0), child: new Text("Enter your new code", textAlign: TextAlign.center)),
              new Expanded(child:new GridView.count
              (
                crossAxisCount: 2,
                children: <Widget>
                [
                  new InkWell(onTap: () { setState(() => tapsTest.add(0)); verifyTaps();}),
                  new InkWell(onTap: () { setState(() => tapsTest.add(1)); verifyTaps(); }),
                  new InkWell(onTap: () { setState(() => tapsTest.add(2)); verifyTaps(); }),
                  new InkWell(onTap: () { setState(() => tapsTest.add(3)); verifyTaps(); }),
                ]
              ))
            ]
          )
        );
        break;
    }
  }

  void verifyTaps()
  {
    if(tapsTest.length >= knocks.length)
    {
      for(int i = 0; i < knocks.length; i++)
      {
        if(knocks[knocks.length - 1 - i] != tapsTest[tapsTest.length - 1 - i])
        {
          return;
        }
      }

      savePinAndPop();
    }
  }

  savePinAndPop() async
  {
    var prefs = await SharedPreferences.getInstance();

    prefs.setInt("pinType", typeOfPin);

    if(typeOfPin == 0 || typeOfPin == 1) prefs.setString("pinCode", code);
    else if(typeOfPin == 3)
    {
      List<String> pinCode = new List<String>();
      for(int i = 0; i < knocks.length; i++)
      {
        pinCode.add(knocks[i].toString());
      }
      
      prefs.setStringList("pinCode", pinCode);
    }

    await prefs.commit();
    Navigator.of(context).pop(typeOfPin);
  }

  void gotoNextPage()
  {
    pageController.nextPage(duration: new Duration(milliseconds: 200), curve: Curves.easeInOut);
  }
}