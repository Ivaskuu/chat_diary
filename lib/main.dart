// Checking for today's DailyChat
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:convert';
import 'dart:io';
import 'dart:async';
import 'dart:math';

import 'dailychat.dart';
import 'dailychat_view.dart';

import 'settings/settings.dart';
import 'infos_view.dart';
import 'onboard.dart';
import 'insert_pin_view.dart';

import 'new_image.dart';
import 'byrefstring.dart';
import 'package:image_picker/image_picker.dart';
import 'week_review/week_review_view.dart';

DateTime installDate; // Needed for adding a new DailyChat

// There will be inserted as many null dailychatscards as the days difference
// between installDate days and dailyChatCards.length
// On adding a missing DailyChatCard, it will be placed in dailyChatsCards[lastNullDailyChatCard]
// and lastNullDailyChatCard will decrease by 1.
// The new card will be added to the top, but will be ordered by date on customIO.cards_save()
int lastNullDailyChatCard;

void main()
{
  runApp
  (
    new MaterialApp
    (
      home: new MainLayout(),
      title: "stry.",
      color: Colors.white
    )
  );
}

List<DailyChatCard> dailyChatsCards = new List<DailyChatCard>();
List<List<DailyChat>> weeks = new List<List<DailyChat>>();

CustomIO customIO;
MainLayoutState mainInstance;
bool showFab = false;
SharedPreferences prefs;

class MainLayout extends StatefulWidget
{
  @override
  State createState() => new MainLayoutState(this);
}

class MainLayoutState extends State<MainLayout>
{ 
  MainLayout mainLayout;
  MainLayoutState(this.mainLayout);

  @override
  initState()
  {
    mainInstance = this;

    customIO = new CustomIO(mainLayout);
    customIO.checkInstallDate().then((_)
    {
      loadSharedPrefs().then
      (
        (_)
        {
          if(prefs.getBool("enablePin") != null && prefs.getBool("enablePin") == true
          && prefs.getBool("onAppLaunchPin") != null && prefs.getBool("onAppLaunchPin") == true)
          {
            Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new InsertPinView())).then((bool result)
            {
              if(result != null)
              {
                customIO.loadDailyChats().then((_) => setState(
                ()
                  {
                    lastNullDailyChatCard = customIO.getMissingDailyChatCardsNum();

                    if(lastNullDailyChatCard >= 0) showFab = true;

                    for(int i = 0; i <= lastNullDailyChatCard; i++)
                    {
                      dailyChatsCards.add(new DailyChatCard(null));
                    }
                    customIO.copyLoadedDailyChatCards();
                  }
                ));
              }
            });
          }
          else
          {
            customIO.loadDailyChats().then((_) => setState(
            ()
              {
                lastNullDailyChatCard = customIO.getMissingDailyChatCardsNum();

                if(lastNullDailyChatCard >= 0) showFab = true;

                for(int i = 0; i <= lastNullDailyChatCard; i++)
                {
                  dailyChatsCards.add(new DailyChatCard(null));
                }
                customIO.copyLoadedDailyChatCards();
              }
            ));
          }
        }
      );
    });

    super.initState();
  }

  loadSharedPrefs() async
  {
    prefs = await SharedPreferences.getInstance();
    return;
  }

  @override
  Widget build (BuildContext context)
  {
    return new Scaffold
    (
      appBar: new AppBar
      (
        backgroundColor: Colors.white,
        title: new Text("stry.", style: new TextStyle(color: Colors.black)),
        elevation: 1.5,
        actions: <Widget>
        [
          new PopupMenuButton
          (
            icon: new Icon(Icons.more_vert, color: Colors.black),
            itemBuilder: (BuildContext context) => <PopupMenuEntry<int>>
            [
              new PopupMenuItem(child: new Row(children: [new Icon(Icons.open_in_browser), new Container(margin: new EdgeInsets.only(left: 24.0), child: new Text("Onboard"))]), value: 0),
              new PopupMenuItem(child: new Row(children: [new Icon(Icons.calendar_today), new Container(margin: new EdgeInsets.only(left: 24.0), child: new Text("Weeks review"))]), value: 1),
              new PopupMenuItem(child: new Row(children: [new Icon(Icons.settings), new Container(margin: new EdgeInsets.only(left: 24.0), child: new Text("Settings"))]), value: 2),
              new PopupMenuItem(child: new Row(children: [new Icon(Icons.settings), new Container(margin: new EdgeInsets.only(left: 24.0), child: new Text("About"))]), value: 3),
            ],
            onSelected: (int val)
            {
              switch(val)
              {
                case 0: // Onboard
                  Navigator.of(context).pushReplacement(new MaterialPageRoute(builder: (_) => new Onboard()));
                  break;
                case 1: // Weeks review
                  computeWeeks();
                  Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new WeekReviewView(weeks))); // TODO: Verify the weeks have finished
                  break;
                case 2: // Settings
                  Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new Settings())).then((_) => loadSharedPrefs());
                  break;
                case 3: // About
                  Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new InfosView()));
                  break;
                default:
                  print("cos");
                  break;
              }
            }
          )
        ]
      ),
      body: new ListView.builder
      (
        scrollDirection: Axis.vertical,
        itemBuilder: (_, int index) => dailyChatsCards[index],
        itemCount: dailyChatsCards.length
      ),
      floatingActionButton: newDailyChatFAB()
    );
  }

  Widget newDailyChatFAB()
  {
    if(showFab)
    {
      return new FloatingActionButton
      (
        backgroundColor: Colors.black,
        tooltip: "Add a new DailyChat",
        child: new Icon(Icons.add, color: Colors.white),
        onPressed: () => addNewDailyChat()
      );
    }
    
    return null;
  }

  // Show a date picker and add a DailyChat(r) for that day
  void addNewDailyChat()
  {
    if(true)
    {
      showDatePicker
      (
        context: context,
        firstDate: installDate,
        initialDate: getInitialDate(),
        lastDate: new DateTime.now(),
        selectableDayPredicate: (DateTime dateTime) => checkDate(dateTime)
      ).then((result)
      {
        if(result != null)
        {
          DailyChat newDailyChat = new DailyChat
          (
            dateDayName: new DateFormat("EEEE").format(result),
            dateDayOfMonth: new DateFormat("d").format(result),
            dateMonth: new DateFormat("MMMM").format(result),
            dateYear: result.year,
            imagePath: "",
            imageTurns: 0,
            moodId: 2,
            title: getRandomDailyChatTitle(),
            chats: new List<String>(),
            chatsPos: new List<bool>()
          );

          setState(
          ()
            {
              dailyChatsCards[lastNullDailyChatCard].child.updateDc(newDailyChat);
              lastNullDailyChatCard == 0 ? showFab = false : lastNullDailyChatCard--;

              customIO.saveDailyChats();
            }
          );
        }
      });
    }
  }

  // Returns the initial date to the last un-added DailyChat
  DateTime getInitialDate()
  {
    DateTime dateTime = new DateTime.now();

    while(!checkDate(dateTime))
    {
      dateTime = dateTime.subtract(new Duration(days: 1));
      print(dateTime.toString());
    }

    return dateTime;
  }

  // Returns true if there isn't a daiychat with that date
  bool checkDate(DateTime dateTime)
  {
    for(int i = 0; i < dailyChatsCards.length; i++)
    {
      if(dailyChatsCards[i].dailyChat != null)
      {
        DateFormat date = new DateFormat("EEEE, d MMMM yyyy");
        DateTime dailyChatDate = date.parse(dailyChatsCards[i].dailyChat.getCompleteDate(true));

        if(dailyChatDate.month == dateTime.month && dailyChatDate.day == dateTime.day)
        {
          return false;
        }
      }
    }
    return true;
  }

  // Returns a randomstring for a DailyChat title
  String getRandomDailyChatTitle()
  {
    Random rand = new Random();
    List<String> titles =
    [
      "An amazing day",
      "An unforgettable day",
      "A crazy day",
      "An unexcepted event",
      "A mysterious day",
      "Unbelievable day",
      "Surprising day",
      "A wonderful journey",
      "A remarkable day",
      "An unimaginalbe day",
      "The perfect day",
      "A marvellous moment"
    ];

    return titles[rand.nextInt(titles.length)];
  }

  // Show the FAB if there aren't DailyChats for all the days since the install date
  void checkShowFab()
  {
    showFab = false;
    int difference;
    int daysNum = new DateTime.now().difference(installDate).inDays; // The number of days since the install date

    if(dailyChatsCards[0] == null) // WTF ?! Why ? // This is what happens when you don't comment your code
    {
      difference = daysNum - (dailyChatsCards.length - 1);
    }
    else
    {
      difference = daysNum - dailyChatsCards.length;
    }

    if(difference > 0) showFab = true;
  }
  
  // TODO: "Your story continues..."
  // UPDATE : The dailyChatsCards are not arranged the way I believed - oops - so the solution is to do a for loop for each day
  void computeWeeks()
  {
    DateTime dateCheck = installDate;
    bool isItMonday;
    int weekNum = 0;
    int daysNum = new DateTime.now().difference(installDate).inDays + 1; // +1 for today's day
    
    weeks.clear();
    weeks.add(new List<DailyChat>()); // Initializing the weeks array
    
    for(int i = 0; i < daysNum; i++)
    {
      isItMonday = dateCheck.weekday == DateTime.MONDAY ? true : false;
      
      if(isItMonday && i != 0) // It is possible that installDate is a monday, but we don't want a new week
      {
        weekNum++;
        weeks.add(new List<DailyChat>());
      }

      DailyChat weekDailyChat;
      // Try to find a dailychat with that date
      for(int i = 0; i < dailyChatsCards.length; i++)
      {
        if(dailyChatsCards[i].dailyChat != null)
        {
          DateTime dcDateTime = new DateTime.fromMillisecondsSinceEpoch(dailyChatsCards[i].dailyChat.getEpochMilliseconds());
          if(dcDateTime.day == dateCheck.day
            && dcDateTime.month == dateCheck.month
            && dcDateTime.year == dateCheck.year)
          {
            weekDailyChat = dailyChatsCards[i].dailyChat;
            break;
          }
        }
      }
      
      weeks[weekNum].add(weekDailyChat);
      dateCheck = dateCheck.add(new Duration(days: 1));
    }
  }
}

class DailyChatCard extends StatefulWidget
{
  DailyChat dailyChat;
  DailyChatCard(this.dailyChat);

  DailyChatCardState child;

  @override
  State createState()
  {
    child = new DailyChatCardState(this.dailyChat, this);
    return child;
  }
}

class DailyChatCardState extends State<DailyChatCard>
{
  // Constructor variables
  DailyChatCard parent;
  DailyChat dailyChat;

  DailyChatCardState(this.dailyChat, this.parent);

  @override
  Widget build (BuildContext context)
  {
    if(dailyChat != null)
    {
      return new SizedBox.fromSize
      (
        size: new Size.fromHeight(200.0),
        child: new Stack
        (
          alignment: FractionalOffset.center,
          fit: StackFit.expand,
          children: <Widget>
          [
            new RotatedBox
            (
              quarterTurns: dailyChat.imageTurns,
              child: new Container
              (
                decoration: new BoxDecoration
                (
                  image: new DecorationImage
                  (
                    image: dailyChat.getImage(), fit: BoxFit.cover,
                    colorFilter: new ColorFilter.mode(new Color.fromARGB(50, 0, 0, 0), BlendMode.luminosity)
                  )
                )
              )
            ),
            new InkWell
            (
              child: new Container
              (
                margin: new EdgeInsets.symmetric(horizontal: 20.0),
                child: new Column
                (
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>
                  [
                    new Text(dailyChat.title, textAlign: TextAlign.center, maxLines: 3, style: new TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 22.0)),
                    new Container
                    (
                      margin: new EdgeInsets.only(top: 6.0),
                      child: new Text(dailyChat.getCompleteDate(false), style: new TextStyle(color: Colors.white, fontWeight: FontWeight.w300, letterSpacing: 1.7))
                    )
                  ]
                )
              ),
              onTap: ()
              {
                if(prefs.getBool("enablePin") != null && prefs.getBool("enablePin") == true
                && prefs.getBool("onDailyChatOpenPin") != null && prefs.getBool("onDailyChatOpenPin") == true)
                {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new InsertPinView())).then((bool result)
                  {
                    if(result != null)
                    {
                      Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new DailyChatView(parent.dailyChat, customIO))).whenComplete(()
                      {
                        // mainLayout.child.updateCards();
                        parent.dailyChat = dailyChat;
                        setState(() {});
                        customIO.saveDailyChats();
                      });
                    }
                  });
                }
                else
                {
                  Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new DailyChatView(parent.dailyChat, customIO))).whenComplete(()
                  {
                    // mainLayout.child.updateCards();
                    parent.dailyChat = dailyChat;
                    setState(() {});
                    customIO.saveDailyChats();
                  });
                }
              },
              onLongPress: () => showOptions(),
            )
          ]
        )
      );
    }
    else
    {
      return new Container();
    }
  }

  void updateDc(DailyChat newDc)
  {
    setState(()
    {
      parent.dailyChat = newDc;
      dailyChat = newDc;
    });
  }

  Future<Null> showOptions() async
  {
    switch
    (
      await showDialog<int>
      (
        context: context,
        child: new SimpleDialog
        (
          children: <Widget>
          [
            new Container
            (
              margin: new EdgeInsets.symmetric(vertical: 8.0),
              child: new FlatButton
              (
                child: new Row(children: <Widget>
                [
                  new Container(margin: new EdgeInsets.only(right: 18.0), child: new Icon(Icons.edit)),
                  new Text("Rename title")
                ]),
                onPressed: () => Navigator.of(context).pop(0)
              )
            ),
            new Divider(),
            new Container
            (
              margin: new EdgeInsets.symmetric(vertical: 8.0),
              child: new FlatButton
              (
                child: new Row(children: <Widget>
                [
                  new Container(margin: new EdgeInsets.only(right: 18.0),child: new Icon(Icons.add_a_photo)),
                  new Text("Choose new image")
                ]),
                onPressed: () => Navigator.of(context).pop(2)
              )
            ),
            new Container
            (
              margin: new EdgeInsets.symmetric(vertical: 8.0),
              child: new FlatButton
              (
                child: new Row(children: <Widget>
                [
                  new Container(margin: new EdgeInsets.only(right: 18.0), child: new Icon(Icons.rotate_left)),
                  new Text("Rotate the image")
                ]),
                onPressed: dailyChat.imagePath != null && dailyChat.imagePath != "" ? () => Navigator.of(context).pop(1) : null
              )
            ),
            new Container
            (
              margin: new EdgeInsets.symmetric(vertical: 8.0),
              child: new FlatButton
              (
                child: new Row(children: <Widget>
                [
                  new Container(margin: new EdgeInsets.only(right: 18.0), child: new Icon(Icons.close)),
                  new Text("Remove the image")
                ]),
                onPressed: dailyChat.imagePath != null && dailyChat.imagePath != "" ? () => Navigator.of(context).pop(3) : null
              )
            ),
            new Divider(),
            new Container
            (
              margin: new EdgeInsets.symmetric(vertical: 8.0),
              child: new FlatButton
              (
                child: new Row(children: <Widget>
                [
                  new Container(margin: new EdgeInsets.only(right: 18.0), child: new Icon(Icons.delete)),
                  new Text("Delete DailyChat")
                ]),
                onPressed: () => Navigator.of(context).pop(4)
              )
            )
          ]
        )
      )
    )
    {
      case 0: // Rename
        renameTitle();
        break;
      case 1: // Rotate image
        rotateImage();
        break;
      case 2: // New image
        newImage();
        break;
      case 3: // Remove image
        setState(() { dailyChat.imagePath = null; dailyChat.imageTurns = 0; customIO.saveDailyChats(); });
        break;
      case 4: // Delete
        askForDailyChatDelete();
        break;
    }
  }

  renameTitle() async
  {
    TextEditingController controller = new TextEditingController();
    controller.text = dailyChat.title;
    showDialog<bool>
    (
      context: context,
      child: new AlertDialog
      (
        title: new Text("Rename DailyChat"),
        content: new TextField(autofocus: false, decoration: new InputDecoration(hintText: "New title"), controller: controller, onSubmitted: (_) => Navigator.of(context).pop(true)),
        actions:
        [
          new FlatButton(child: new Text("CANCEL"), onPressed: () => Navigator.of(context).pop(false)),
          new FlatButton(child: new Text("RENAME"), onPressed: () => Navigator.of(context).pop(true))
        ]
      )
    ).then((bool result)
    {
      if(result != null && result)
      {
        if(controller.text.trim() != "")
        {
          setState(() => dailyChat.title = controller.text.trim());
          customIO.saveDailyChats();
        }
      }
    });
  }
  
  rotateImage()
  {
    ByRefString turns = new ByRefString(dailyChat.imageTurns.toString());
    Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new NewImage(new Image.file(new File(dailyChat.imagePath)), turns))).whenComplete(()
    {
      setState(() { dailyChat.imageTurns = int.parse(turns.string); });
      customIO.saveDailyChats();
    });
  }

  newImage() async
  {
    var imageFile = await ImagePicker.pickImage();
    setState(()
    {
      ByRefString turns = new ByRefString("0");
      Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new NewImage(new Image.file(imageFile), turns))).whenComplete(()
      {
        setState(()
        {
          dailyChat.imagePath = imageFile.path;
          dailyChat.imageTurns = int.parse(turns.string);
        });
        customIO.saveDailyChats();
      });
    });
  }

  askForDailyChatDelete()
  {
    showDialog
    (
      context: context,
      child: new AlertDialog
      (
        title: new Text("Do you really want to delete the DailyChat ?"),
        content: new Text("All your conversation (messages) in this DailyChat will be deleted and you will not be able to recover them."),
        actions:
        [
          new FlatButton(child: new Text("CANCEL"), onPressed: () => Navigator.of(context).pop()),
          new FlatButton(child: new Text("DELETE"), onPressed: ()
          {
            setState(() => dailyChat = null);
            parent.dailyChat = null;
            customIO.saveDailyChats();
            
            lastNullDailyChatCard++;
            mainInstance.setState(() => showFab = true);

            Navigator.of(context).pop();
          })
        ]
      )
    );
  }
}

class CustomIO
{
  MainLayout mainLayout;
  CustomIO(this.mainLayout);

  List<DailyChatCard> cards = new List<DailyChatCard>();

  Future deleteDailychatsFile() async
  {
    // Get the path to the document directory.
    await new File("${(await getApplicationDocumentsDirectory()).path}/dailychats.bin").delete();
    dailyChatsCards.clear();
    
    return true;
  }

  // Overwrites or creates the dailychats.bin file with JSON parsed DailyChats
  saveDailyChats() async
  {
    // Opening the file
    File dailyChatsFile = new File("${(await getApplicationDocumentsDirectory()).path}/dailychats.bin");
    dailyChatsFile.createSync(recursive: true);

    // Sorting the list based on the epoch time
    List dailyChatsToSave = new List.from(dailyChatsCards);
    dailyChatsToSave.removeWhere((value) => value.dailyChat == null);
    dailyChatsToSave.sort((DailyChatCard a, DailyChatCard b) => b.dailyChat.getEpochMilliseconds().compareTo(a.dailyChat.getEpochMilliseconds()));

    List dailyChatsToJson = new List();
    for(int i = 0; i < dailyChatsToSave.length; i++)
    {
      dailyChatsToJson.add(dailyChatToJSON(dailyChatsToSave[i].dailyChat));
    }

    dailyChatsFile.writeAsStringSync(JSON.encode(dailyChatsToJson));
  }

  // Reads the DailyChats file and adds them in the cards array
  loadDailyChats() async
  {
    File dailyChatsFile = new File("${(await getApplicationDocumentsDirectory()).path}/dailychats.bin");
    dailyChatsFile.createSync(recursive: true);
    String dailyChtasJSON = dailyChatsFile.readAsStringSync();

    if(dailyChtasJSON.length > 0)
    {
      List<Map> decodedDailyChats = JSON.decode(dailyChtasJSON);
      if(decodedDailyChats.length > 0) customIO.jsonToDailyChats(decodedDailyChats);
    }
  }

  // If the DailyChats are already loaded, copy the cards array into the dailyChatsCards array
  copyLoadedDailyChatCards()
  {
    if(cards != null)
    {
      for(int i = 0; i < cards.length; i++)
      {
        dailyChatsCards.add(cards[i]);
      }
    }
  }

  // Returns a JSON parsed DailyChat, to save
  dailyChatToJSON(DailyChat dc)
  {
    return {"title": dc.title, "date": dc.getCompleteDate(true), "imagePath": dc.imagePath, "imageTurns": dc.imageTurns, "moodId": dc.moodId, "chats": dc.chats, "chatsPos": dc.chatsPos};
  }

  // Parse the JSON into a DailyChat then add it to the cards array
  jsonToDailyChats(List<Map> list)
  {
    for(int i = 0; i < list.length; i++)
    {
      DailyChat newDailyChat = DailyChat.newFromJSON(list[i]);
      cards.add(new DailyChatCard(newDailyChat));
    }
  }

  // Sets the value of the global var installDate
  checkInstallDate() async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    // The first time the user launches the app
    if(prefs.getString("installDate") == null || prefs.getString("installDate") == "")
    {
      prefs.setString("installDate", new DateTime.now().millisecondsSinceEpoch.toString());
      await prefs.commit();
      
      installDate = new DateTime.now();
    }
    else
    {
      installDate = new DateTime.fromMillisecondsSinceEpoch(int.parse(prefs.getString("installDate")));
    }

    return;
  }

  // Returns the difference between the days since the install date and the number of DailyChats created
  int getMissingDailyChatCardsNum()
  {
    int daysNum = new DateTime.now().difference(installDate).inDays;
    return daysNum - cards.length;
  }
}
