import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'dart:async';
import 'dart:convert';

import 'main.dart';
import 'dailychat.dart';
import 'new_image.dart';
import 'image_view.dart';
import 'byrefstring.dart';

import 'package:intl/intl.dart';

bool chatEdit;

class DailyChatEditChat extends StatefulWidget
{
  DailyChat dailyChat;
  bool editChats;
  CustomIO io;
  DailyChatEditChat(this.dailyChat, this.editChats, this.io);

  DailyChatEditChatState child;

  @override
  State createState()
  {
    child = new DailyChatEditChatState(dailyChat, editChats, io);
    return child;
  }
}

DailyChatEditChatState instance;

class DailyChatEditChatState extends State<DailyChatEditChat>
{
  DailyChat dailyChat;
  bool editChats;
  CustomIO io;
  DailyChatEditChatState(this.dailyChat, this.editChats, this.io);

  List<ChatBubble> chatMsg;
  TextEditingController controller = new TextEditingController();

  ChatBubble lastDeletedMessage;
  int lastDeletedMessagePos;
  bool lastDeletedMessageDir;

  @override
  void initState()
  {
    instance = this;
    chatEdit = editChats;

    updateChats();
    super.initState();
  }

  void updateChats()
  {
    chatMsg = new List<ChatBubble>();
    for(int i = 0; i < dailyChat.chats.length; i++)
    {
      var decodedJSON = JSON.decode(dailyChat.chats[i]);

      if(decodedJSON["type"] == "image")
      {
        chatMsg.add(new ChatBubble(dailyChat.chatsPos[i], decodedJSON["message"], imagePath: decodedJSON["imagePath"], turns: decodedJSON["turns"]));
      }
      else chatMsg.add(new ChatBubble(dailyChat.chatsPos[i], decodedJSON["message"]));
    }
  }

  @override
  Widget build (BuildContext context)
  {
    return new Column
    (
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>
      [
        new Expanded
        (
          child: getListOrEmptyState()
        ),
        chatTextField()
      ]
    );
  }

  Widget getListOrEmptyState()
  {
    if(chatMsg.length > 0)
    {
      return new ListView.builder
      (
        scrollDirection: Axis.vertical,
        itemBuilder: (_, int index) => chatMsg[index],
        itemCount: chatMsg.length,
        reverse: true
      );
    }
    else
    {
      return new Container
      (
        margin: new EdgeInsets.all(50.0),
        child: new Column
        (
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>
          [
            new Icon(Icons.sentiment_satisfied, color: Colors.grey, size: 40.0),
            new Container
            (
              margin: new EdgeInsets.only(top: 26.0),
              child: new Text("No chats to show. + Add new chats and see them here.", textAlign: TextAlign.center, style: Theme.of(context).textTheme.headline.copyWith(color: Colors.grey)),
            )
          ]
        )
      );
    }
  }

  Widget chatTextField()
  {
    if(editChats)
    {
      return new IconTheme
      (
        data: new IconThemeData(color: Colors.black),
        child: new Container
        (
          color: Colors.white,
          child: new Row
          (
            children: <Widget>
            [
              new Container
              (
                margin: const EdgeInsets.only(right: 4.0),
                child: new GestureDetector
                (
                  onVerticalDragStart: (_) { showMediaBottomSheet(true); HapticFeedback.vibrate(); },
                  child: new RotatedBox
                  (
                    quarterTurns: 2,
                    child: new IconButton
                    (
                      icon: new Icon(Icons.send),
                      onPressed: () => submitMessage(true)
                    )
                  )
                )
              ),
              new Expanded
              (
                child: new TextField
                (
                  controller: controller,
                  keyboardType: TextInputType.text,
                  decoration: new InputDecoration(hintText: "Write a message"),
                  onChanged: (String newMsg) => newMsg != "" ? showWritingBubble() : deleteWritingBubble(),
                )
              ),
              new Container
              (
                margin: const EdgeInsets.symmetric(horizontal: 4.0),
                child: new GestureDetector
                (
                  onVerticalDragStart: (_) { showMediaBottomSheet(false); HapticFeedback.vibrate(); },
                  child: new IconButton
                  (
                    icon: new Icon(Icons.send),
                    onPressed: () => submitMessage(false)
                  )
                )
              ),
            ]
          )
        )
      );
    }
    else return new Container();
  }

  void showWritingBubble()
  {
    if(chatMsg.length == 0 || chatMsg[0].text != null)
    setState(()
    {
      chatMsg.insert(0, new ChatBubble(false, null));
    });
  }

  void deleteWritingBubble()
  {
    if(chatMsg[0].imagePath == null && chatMsg[0].text == null)
    setState(()
    {
      chatMsg.removeAt(0);
    });
  }

  void submitMessage(bool dir)
  {
    if(controller.text.trim() != "")
    {
      deleteWritingBubble();

      setState(()
      {
        String msg = JSON.encode({"type": "only_text", "message": controller.text.trim()});
        dailyChat.chats.insert(0, msg);
        dailyChat.chatsPos.insert(0, dir);

        chatMsg.insert(0, new ChatBubble(dir, controller.text.trim()));
        controller.clear();

        io.saveDailyChats();
      });
    }
  }

  void deleteMessage(ChatBubble chat, bool msgDir)
  {
    if(editChats)
    {
      lastDeletedMessage = chat;
      lastDeletedMessageDir = msgDir;
      lastDeletedMessagePos = chatMsg.indexOf(chat);

      setState(()
      {
        dailyChat.chats.removeAt(lastDeletedMessagePos);
        dailyChat.chatsPos.removeAt(lastDeletedMessagePos);
        chatMsg.removeAt(lastDeletedMessagePos);

        io.saveDailyChats();
      });
    }
  }

  void recoverDeletedMessage()
  {
    if(lastDeletedMessage != null)
    {
      setState(()
      {
        if(lastDeletedMessage.imagePath == null || lastDeletedMessage.imagePath == "") // Only text message
        {
          String chatToInsert = JSON.encode({"type": "only_text", "message": lastDeletedMessage.text});

          dailyChat.chats.insert(lastDeletedMessagePos, chatToInsert);
          dailyChat.chatsPos.insert(lastDeletedMessagePos, lastDeletedMessageDir);

          chatMsg.insert(lastDeletedMessagePos, lastDeletedMessage);
        }
        else // Image message
        {
          String chatToInsert = JSON.encode({"type": "image", "imagePath": lastDeletedMessage.imagePath, "message": lastDeletedMessage.text, "turns": lastDeletedMessage.turns});

          dailyChat.chats.insert(lastDeletedMessagePos, chatToInsert);
          dailyChat.chatsPos.insert(lastDeletedMessagePos, lastDeletedMessageDir);

          chatMsg.insert(0, new ChatBubble(lastDeletedMessageDir, lastDeletedMessage.text, imagePath: lastDeletedMessage.imagePath, turns: lastDeletedMessage.turns));
        }

        // Resetting the last deleted message
        lastDeletedMessage = null;
        lastDeletedMessageDir = null;
        lastDeletedMessagePos = null;
      });
    }
  }

  showMediaBottomSheet(bool dir)
  {
    showModalBottomSheet<int>
    (
      context: context,
      builder: (BuildContext context) => new SendMediaBottomSheet()
    ).then((int value)
    {
      if(value != null)
      {
        if(value == 0) newImage(dir);
        else if(value == 1) newInfoMessage();
        else showDialog(barrierDismissible: false, context: context, child: new AlertDialog
        (
          title: new Text("Attention"),
          content: new Text("This feature is not yet implemented, but the dev is working hard so stay tuned !"),
          actions: <Widget>[new FlatButton(child: new Text("SAD FACE"), onPressed: () => Navigator.of(context).pop())],
        ));
      }
    });
  }

  newImage(bool dir) async
  {
    var imageFile = await ImagePicker.pickImage();
    setState(()
    {
      String chatToInsert;
      ByRefString imageText = new ByRefString("");
      ByRefString turns = new ByRefString("0");

      // TODO: Could yield an error on exit without adding an image
      Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new NewImage(new Image.file(imageFile), turns, text: imageText))).whenComplete(()
      {
        chatToInsert = JSON.encode({"type": "image", "imagePath": imageFile.path, "message": imageText.string.trim(), "turns": int.parse(turns.string)});

        deleteWritingBubble();

        dailyChat.chats.insert(0, chatToInsert);
        dailyChat.chatsPos.insert(0, dir);

        chatMsg.insert(0, new ChatBubble(dir, imageText.string.trim(), imagePath: imageFile.path, turns: int.parse(turns.string)));
        io.saveDailyChats();
      });
    });
  }

  newInfoMessage()
  {
    chatMsg.insert(0, new ChatBubble(null, "You have changed the title from 'Tua madre quella troia' to 'Una bella giornata'"));
  }
}

class ChatBubble extends StatelessWidget
{
  bool left;
  String text;
  String imagePath;
  int turns;

  ChatBubble(this.left, this.text, {this.imagePath, this.turns});

  EdgeInsets _chatBubbleMargin;
  EdgeInsets edgeInsets;
  CrossAxisAlignment crossAxisAlignment;
  BuildContext context;

  @override
  Widget build (BuildContext ctx)
  {
    context = ctx;
    Widget widgetToReturn;

    if(left == null)
    {
      edgeInsets = new EdgeInsets.only(right: 64.0, left: 64.0);
      crossAxisAlignment = CrossAxisAlignment.center;
    }
    else if(left)
    {
      edgeInsets = new EdgeInsets.only(right: 64.0);
      crossAxisAlignment = CrossAxisAlignment.start;
    }
    else if(!left)
    {
      edgeInsets = new EdgeInsets.only(left: 64.0);
      crossAxisAlignment = CrossAxisAlignment.end;
    }

    if(left == null) widgetToReturn = infoMessage(); // Info message
    else if(imagePath == null && (text == null || text == "")) widgetToReturn = writingBubble(); // Only text chat bubble
    else if(imagePath == null) widgetToReturn = onlyTextChatBubble(); // Only text chat bubble
    else if (text != null && text != "") widgetToReturn = textAndImageChatBubble(); // Image + text
    else widgetToReturn = onlyImageChatBubble(); // Only image

    return new Container
    (
      margin: edgeInsets,
      child: new Column
      (
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: crossAxisAlignment,
        children: <Widget>
        [
          widgetToReturn
        ]
      )
    );
  }

  Widget inkwell(Widget child)
  {
    return new InkWell
    (
      onTap: ()
      {
        if(imagePath != null)
          Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new ImageView
          (
            new Image.file(new File(imagePath), fit: BoxFit.scaleDown), turns, text: text))
          );
        else
        {
          Clipboard.setData(new ClipboardData(text: text));
          Scaffold.of(context).showSnackBar
          (
            new SnackBar(content: new Text("Chat copied to clipboard"), duration: new Duration(milliseconds: 700))
          );
        }
      },
      onLongPress: () => chatEdit ? messageOptions() : null,
      child: child
    );
  }

  Widget onlyTextChatBubble()
  {
    return new Container
    (
      margin: _chatBubbleMargin,
      child: new Card
      (
        color: !left ? Colors.blueGrey : null,
        elevation: 1.0,
        child: inkwell(new Container
        (
          margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
          child: new Text
          (
            text, softWrap: true,
            style: text.contains(" ")
              ? new TextStyle(color: !left ? Colors.white : null)
              : new TextStyle(color: !left ? Colors.white : null, fontSize: Theme.of(context).textTheme.body1.fontSize * 2)
          )
        )
      ))
    );
  }

  Widget textAndImageChatBubble()
  {
    return new Container
    (
      margin: _chatBubbleMargin,
      child: new Card
      (
        color: !left ? Colors.blueGrey : null,
        elevation: 1.0,
        child: inkwell(new Container
        (
          margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
          child: new Column
          (
            children: <Widget>
            [
              new RotatedBox(quarterTurns: turns, child: new Image.file(new File(imagePath), fit: BoxFit.scaleDown)),
              new Container
              (
                margin: new EdgeInsets.only(top: 10.0),
                child: new Text(text, style: new TextStyle(color: !left ? Colors.white : null))
              )
            ]
          )
        ))
      )
    );
  }

  Widget onlyImageChatBubble()
  {
    return new Container
    (
      margin: _chatBubbleMargin,
      child: new Card
      (
        elevation: 1.0,
        child: inkwell(new Container
        (
          child: new RotatedBox(quarterTurns: turns, child: new Image.file(new File(imagePath), fit: BoxFit.scaleDown))
        )
      ))
    );
  }

  Widget infoMessage()
  {
    return new Container
    (
      margin: _chatBubbleMargin,
      child: new Card
      (
        color: Colors.grey,
        elevation: 0.5,
        child: inkwell(new Container
        (
          margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
          child: new Text
          (
            text, softWrap: true, textAlign: TextAlign.center,
            style: new TextStyle(color: Colors.white)
          )
        )
      ))
    );
  }

  Widget writingBubble()
  {
    return new Container
    (
      margin: _chatBubbleMargin,
      child: new Card
      (
        color: !left ? Colors.blueGrey : null,
        elevation: 1.0,
        child: inkwell(new Container
        (
          margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
          child: new Text
          (
            " . . . ", softWrap: true,
            style: new TextStyle(color: !left ? Colors.white : null, fontSize: Theme.of(context).textTheme.body1.fontSize * 1.5)
          )
        )
      ))
    );
  }

  Future<Null> messageOptions() async
  {
    switch
    (
      await showDialog<int>
      (
        context: context,
        child: new SimpleDialog
        (
          children: <Widget>
          [
            new Container
            (
              margin: new EdgeInsets.symmetric(vertical: 8.0),
              child: new FlatButton
              (
                onPressed: () => Navigator.of(context).pop(0), child: new Row(children: <Widget>
                [
                  new Container(margin: new EdgeInsets.only(right: 18.0), child: new Icon(Icons.delete)),
                  new Text("Delete message")
                ])
              )
            ),
            new Divider(),
            new Container
            (
              margin: new EdgeInsets.symmetric(vertical: 8.0),
              child: new FlatButton
              (
                child: new Row(children: <Widget>
                [
                  new Container(margin: new EdgeInsets.only(right: 18.0), child: new Icon(Icons.star)),
                  new Text("Set message as important")
                ]),
                onPressed: null
              )
            )
          ]
        )
      )
    )
    {
      case 0:
        instance.deleteMessage(this, left); Scaffold.of(context).showSnackBar
        (
          new SnackBar(content: new Text("The chat was deleted"), action: new SnackBarAction(label: "UNDO", onPressed: ()
          {
            instance.recoverDeletedMessage();
          }))
        );
        break;
      case 1:
        break;
    }
  }
}

class SendMediaBottomSheet extends StatelessWidget
{
  PageController controller = new PageController();

  @override
  Widget build(BuildContext context)
  {
    return new PageView
    (
      controller: controller,
      scrollDirection: Axis.horizontal,
      children: <Widget>
      [
        new GridView.count
        (
          scrollDirection: Axis.vertical,
          crossAxisCount: 2,
          children: <Widget>
          [
            new Column
            (
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>
              [
                new IconButton(icon: new Icon(Icons.image), iconSize: 45.0, onPressed: () => Navigator.of(context).pop(0), color: Colors.black),
                new Container(margin: new EdgeInsets.only(top: 8.0), child: new Text("Send an image"))
              ]
            ),
            new Column
            (
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>
              [
                new IconButton(icon: new Icon(Icons.local_movies), iconSize: 45.0, onPressed: () => Navigator.of(context).pop(1), color: Colors.black),
                new Container(margin: new EdgeInsets.only(top: 8.0), child: new Text("Send a video"))
              ]
            ),
            new Column
            (
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>
              [
                new IconButton(icon: new Icon(Icons.wb_sunny), iconSize: 45.0, onPressed: () => Navigator.of(context).pop(2), color: Colors.black),
                new Container(margin: new EdgeInsets.only(top: 8.0), child: new Text("Send meteo info"))
              ]
            ),
            new IconButton(icon: new Icon(Icons.arrow_forward), iconSize: 40.0, onPressed: () => controller.nextPage(duration: new Duration(milliseconds: 200), curve: Curves.easeInOut), color: Colors.black)
          ]
        ),
        new GridView.count
        (
          scrollDirection: Axis.vertical,
          crossAxisCount: 2,
          children: <Widget>
          [
            new Column
            (
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>
              [
                new IconButton(icon: new Icon(Icons.location_on), iconSize: 45.0, onPressed: () => Navigator.of(context).pop(3), color: Colors.black),
                new Container(margin: new EdgeInsets.only(top: 8.0), child: new Text("Send a location"))
              ]
            ),
            new Column
            (
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>
              [
                new IconButton(icon: new Icon(Icons.headset), iconSize: 45.0, onPressed: () => Navigator.of(context).pop(4), color: Colors.black),
                new Container(margin: new EdgeInsets.only(top: 8.0), child: new Text("Send an audio"))
              ]
            ),
            new IconButton(icon: new Icon(Icons.arrow_back), iconSize: 40.0, onPressed: () => controller.previousPage(duration: new Duration(milliseconds: 200), curve: Curves.easeInOut), color: Colors.black)
          ]
        )
      ]
    );
  }
}

class AI
{
  static String onNewDailyChat()
  {
    DateTime msgDateTime = new DateTime.now();
    if(msgDateTime.hour >= 3 && msgDateTime.hour < 11)
    {
      return "Good morning, Adrian. What are you planning to do today ?";
    }
    else if(msgDateTime.hour >= 11 && msgDateTime.hour < 20)
    {
      return "Good middle of the day Adrian!";
    }
    else
    {
      return "Good afternoon Adrian! How was today ?";
    }
  }
  
  String parseMessage(String msg)
  {
    if(msg.toLowerCase().trim().contains("hey diary")) return "Hey Adrian!";
    
    return null;
  }
}









