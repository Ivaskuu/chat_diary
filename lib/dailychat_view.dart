import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:async';

import 'dailychat_chat_fullscreen.dart';
import 'dailychat_edit_chat.dart';
import 'image_view.dart';

import 'new_image.dart';
import 'byrefstring.dart';
import 'package:image_picker/image_picker.dart';

import 'dailychat.dart';
import 'main.dart';
import 'insert_pin_view.dart';

class DailyChatView extends StatefulWidget
{
  DailyChat _dailyChat;
  CustomIO _io;
  DailyChatView(this._dailyChat, this._io);

  @override
  State createState() => new DailyChatViewState(_dailyChat, _io, this);
}

class DailyChatViewState extends State<DailyChatView>
{
  bool _fullTitle = false;
  DailyChat dailyChat;
  CustomIO io;
  DailyChatView parent;

  DailyChatViewState(this.dailyChat, this.io, this.parent);

  DailyChatEditChat dcec;
  bool editTitle = false;
  TextEditingController dcTitleController = new TextEditingController();

  @override
  initState()
  {
    dcec = new DailyChatEditChat(dailyChat, false, io);
    dcTitleController.text = dailyChat.title;
    super.initState();
  }
  
  @override
  Widget build (BuildContext context)
  {
    return new Scaffold
    (
      appBar: getAppBar(),
      body: dcec,
      floatingActionButton: new FloatingActionButton
      (
        backgroundColor: Colors.black,
        child: new Icon(Icons.chat),
        onPressed: ()
        {
          if(prefs.getBool("enablePin") != null && prefs.getBool("enablePin") == true
          && prefs.getBool("onDailyChatEditPin") != null && prefs.getBool("onDailyChatEditPin") == true)
          {
            Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new InsertPinView())).then((bool result)
            {
              if(result != null)
              {
                Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new DailyChatChatFullscreen(dailyChat, io))).whenComplete(()
                {
                  setState((){dcec.child.setState((){dcec.child.updateChats();});});
                });
              }
            });
          }
          else
          {
            Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new DailyChatChatFullscreen(dailyChat, io))).whenComplete(()
            {
              setState((){dcec.child.setState((){dcec.child.updateChats();});});
            });
          }
        }
      )
    );
  }
  
  getAppBar()
  {
    if(editTitle)
    {
      return new PreferredSize
      (
        preferredSize: new Size.fromHeight(220.0),
        child: new Stack
        (
          children: <Widget>
          [
            new RotatedBox
            (
              quarterTurns: dailyChat.imageTurns,
              child: new Container
              (
                decoration: new BoxDecoration
                (
                  image: new DecorationImage
                  (
                    image: _getImage(), fit: BoxFit.cover
                  )
                )
              )
            ),
            new AppBar
            (
              backgroundColor: Colors.black38,
              leading: new IconButton(icon: new Icon(Icons.close), onPressed: () => setState(() => editTitle = false)),
              title: new Text("Rename DailyChat"),
              actions: <Widget>
              [
                new IconButton(icon: new Icon(Icons.done), onPressed: dcTitleController.text.trim() != "" ? () => setState(()
                {
                  setState(()
                  {
                    dailyChat.title = dcTitleController.text.trim();
                    editTitle = false;
                    io.saveDailyChats();
                  });
                }) : null)
              ]
            ),
            new Container
            (
              padding: new EdgeInsets.all(4.0),
              child: new Column
              (
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>
                [
                  new Column
                  (
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>
                    [
                      new Container
                      (
                        margin: new EdgeInsets.only(bottom: 8.0, top: 73.0),
                        child: new CircleAvatar
                        (
                          child: new GestureDetector
                          (
                            onLongPress: moodIdEdit,
                            child: new Icon(DailyChat.getMoodIcon(dailyChat.moodId), size: 40.0, color: Colors.white)
                          ),
                          radius: 30.0
                        )
                      ),
                      new TextField
                      (
                        controller: dcTitleController,
                        autofocus: true,
                        style: new TextStyle(color: Colors.white, fontSize: 26.0),
                        onChanged: (_) => setState((){}),
                        onSubmitted: (String newTitle) => newTitle.trim() != "" ? setState(() { dailyChat.title = newTitle.trim(); editTitle = false; io.saveDailyChats(); }) : null,
                        //decoration: new InputDecoration(hintText: "Summarize the day in a few words") TODO: toDouble() error
                      ),
                      new Text(dailyChat.getCompleteDate(false), style: new TextStyle(color: Colors.white, fontSize: 16.0))
                    ]
                  )
                ]
              )
            )
          ]
        )
      );
    }
    else
    {
      return new PreferredSize
      (
        preferredSize: new Size.fromHeight(220.0),
        child: new GestureDetector
        (
          onTap: _onAppbarImagePressed,
          onLongPress: imageOptions,
          child: new Stack
          (
            alignment: FractionalOffset.center,
            fit: StackFit.expand,
            children: <Widget>
            [
              new RotatedBox
              (
                quarterTurns: dailyChat.imageTurns,
                child: new Container
                (
                  decoration: new BoxDecoration(image: new DecorationImage(image: _getImage(), fit: BoxFit.cover))
                )
              ),
              new AppBar(backgroundColor: Colors.black12, elevation: 2.0),
              new Column
              (
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>
                [
                  new Container
                  (
                    margin: new EdgeInsets.only(bottom: 24.0),
                    child: new CircleAvatar
                    (
                      child: new GestureDetector
                      (
                        onLongPress: moodIdEdit,
                        child: new Icon(DailyChat.getMoodIcon(dailyChat.moodId), size: 40.0, color: Colors.white)
                      ),
                      radius: 30.0
                    )
                  ),
                  new GestureDetector
                  (
                    onTap: _onTitlePressed,
                    onLongPress: () => setState(() => editTitle = true),
                    child: _getTitle()
                  ),
                  new Container
                  (
                    margin: new EdgeInsets.only(bottom: 30.0, top: 4.0),
                    child: new Text(dailyChat.getCompleteDate(false), style: new TextStyle(color: Colors.white, fontSize: 16.0))
                  )
                ]
              )
            ]
          )
        )
      );
    }
  }

  Widget _getTitle()
  {
    if(_fullTitle)
    {
      return new Container
      (
        margin: new EdgeInsets.symmetric(horizontal: 12.0),
        child: new SingleChildScrollView
        (
          scrollDirection: Axis.horizontal,
          child: new Text
          (
            dailyChat.title,
            style: new TextStyle(color: Colors.white, fontSize: 26.0, fontWeight: FontWeight.w500),
            textAlign: TextAlign.center,
            softWrap: true,
            maxLines: 1
          )
        )
      );
    }
    else
    {
      return new Text
      (
        dailyChat.title,
        style: new TextStyle(color: Colors.white, fontSize: 26.0, fontWeight: FontWeight.w500),
        overflow: TextOverflow.ellipsis,
        textAlign: TextAlign.center,
        softWrap: true,
        maxLines: 1
      );
    }
  }

  void _onTitlePressed() // Switch the title form (ellipsed / full title with scrollview)
  {
    setState(()
    {
      print("[Title pressed, changing its form]");
      _fullTitle = !_fullTitle;
    });
  }

  Future<Null> imageOptions() async
  {
    switch
    (
      await showDialog<int>
      (
        context: context,
        child: new SimpleDialog
        (
          children: <Widget>
          [
            new Container
            (
              margin: new EdgeInsets.symmetric(vertical: 8.0),
              child: new FlatButton
              (
                onPressed: () => Navigator.of(context).pop(0), child: new Row(children: <Widget>
                [
                  new Container(margin: new EdgeInsets.only(right: 18.0), child: new Icon(Icons.add_a_photo)),
                  new Text("Pick a new image")
                ])
              )
            ),
            new Container
            (
              margin: new EdgeInsets.symmetric(vertical: 8.0),
              child: new FlatButton
              (
                child: new Row(children: <Widget>
                [
                  new Container(margin: new EdgeInsets.only(right: 18.0), child: new Icon(Icons.rotate_left)),
                  new Text("Rotate the image")
                ]),
                onPressed: dailyChat.imagePath != null && dailyChat.imagePath != "" ? () => Navigator.of(context).pop(1) : null
              )
            ),
            new Divider(),
            new Container
            (
              margin: new EdgeInsets.symmetric(vertical: 8.0),
              child: new FlatButton
              (
                child: new Row(children: <Widget>
                [
                  new Container(margin: new EdgeInsets.only(right: 18.0), child: new Icon(Icons.close)),
                  new Text("Remove the image")
                ]),
                onPressed: dailyChat.imagePath != null && dailyChat.imagePath != "" ? () => Navigator.of(context).pop(2) : null
              )
            )
          ]
        )
      )
    )
    {
      case 0:
        newImage();
        break;
      case 1: // Show the edit image view to rotate
        ByRefString turns = new ByRefString(dailyChat.imageTurns.toString());
        Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new NewImage(new Image.file(new File(dailyChat.imagePath)), turns))).whenComplete(()
        {
          setState(() { dailyChat.imageTurns = int.parse(turns.string); });
          io.saveDailyChats();
        });
        break;
      case 2:
        setState(() { dailyChat.imagePath = null; dailyChat.imageTurns = 0; });
        io.saveDailyChats();
        break;
    }
  }

  Future<Null> moodIdEdit() async
  {
    int newMood = await showDialog<int>
    (
      context: context,
      child: new SimpleDialog
      (
        children: <Widget>
        [
          new Container
          (
            margin: new EdgeInsets.symmetric(vertical: 8.0),
            child: new FlatButton
            (
              onPressed: () => Navigator.of(context).pop(0), child: new Row(children: <Widget>
              [
                new Container(margin: new EdgeInsets.only(right: 18.0), child: new Icon(Icons.sentiment_very_dissatisfied)),
                new Text("The worst day of my life")
              ])
            )
          ),
          new Container
          (
            margin: new EdgeInsets.symmetric(vertical: 8.0),
            child: new FlatButton
            (
              onPressed: () => Navigator.of(context).pop(1), child: new Row(children: <Widget>
              [
                new Container(margin: new EdgeInsets.only(right: 18.0), child: new Icon(Icons.sentiment_dissatisfied)),
                new Text("Bad day..")
              ])
            )
          ),
          new Container
          (
            margin: new EdgeInsets.symmetric(vertical: 8.0),
            child: new FlatButton
            (
              onPressed: () => Navigator.of(context).pop(2), child: new Row(children: <Widget>
              [
                new Container(margin: new EdgeInsets.only(right: 18.0), child: new Icon(Icons.sentiment_neutral)),
                new Text("Nothing special today")
              ])
            )
          ),
          new Container
          (
            margin: new EdgeInsets.symmetric(vertical: 8.0),
            child: new FlatButton
            (
              onPressed: () => Navigator.of(context).pop(3), child: new Row(children: <Widget>
              [
                new Container(margin: new EdgeInsets.only(right: 18.0), child: new Icon(Icons.sentiment_satisfied)),
                new Text("Great day !")
              ])
            )
          ),
          new Container
          (
            margin: new EdgeInsets.symmetric(vertical: 8.0),
            child: new FlatButton
            (
              onPressed: () => Navigator.of(context).pop(4), child: new Row(children: <Widget>
              [
                new Container(margin: new EdgeInsets.only(right: 18.0), child: new Icon(Icons.sentiment_very_satisfied)),
                new Text("An awesome day !")
              ])
            )
          ),
        ]
      )
    );

    setState(() => newMood != null ? dailyChat.moodId = newMood : null);
    io.saveDailyChats();
  }


  newImage() async
  {
    var imageFile = await ImagePicker.pickImage();
    setState(()
    {
      ByRefString turns = new ByRefString("0");
      Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new NewImage(new Image.file(imageFile), turns))).whenComplete(()
      {
        turns.string != "null"
          ? setState(()
          {
            dailyChat.imagePath = imageFile.path;
            dailyChat.imageTurns = int.parse(turns.string);
          })
          : null;

        io.saveDailyChats();
      });
    });
  }

  void _onAppbarImagePressed()
  {
    Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new ImageView(new Image(image: _getImage(), fit: BoxFit.scaleDown), dailyChat.imageTurns, title: dailyChat.title)));
  }

  ImageProvider _getImage()
  {
    if(dailyChat.imagePath != null && dailyChat.imagePath != "")
      return new Image.file(new File(dailyChat.imagePath), fit: BoxFit.cover).image;

    else return new AssetImage("graphics/activities.png");
  }
}