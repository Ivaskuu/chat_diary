import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:io';

class DailyChat
{
  String title;

  String dateDayName;
  String dateDayOfMonth;
  String dateMonth;
  int dateYear;

  int moodId;
  
  String imagePath;
  int imageTurns;

  List<String> chats;
  List<bool> chatsPos; // true = left, false = right

  DailyChat({this.title, this.dateDayName, this.dateDayOfMonth, this.dateMonth, this.dateYear, this.moodId, this.imagePath, this.imageTurns, this.chats, this.chatsPos});

  String getCompleteDate(bool year)
  {
    if(year) return dateDayName + ", " + dateDayOfMonth + " " + dateMonth + " $dateYear";
    return dateDayName + ", " + dateDayOfMonth + " " + dateMonth;
  }

  int getEpochMilliseconds()
  {
    DateFormat date = new DateFormat("EEEE, d MMMM yyyy");
    return date.parse(getCompleteDate(true)).millisecondsSinceEpoch;
 }

  static IconData getMoodIcon(int id)
  {
    switch(id)
    {
      case 0:
        return Icons.sentiment_very_dissatisfied;
        break;
      case 1:
        return Icons.sentiment_dissatisfied;
        break;
      case 2:
        return Icons.sentiment_neutral;
        break;
      case 3:
        return Icons.sentiment_satisfied;
        break;
      case 4:
        return Icons.sentiment_very_satisfied;
        break;
      default:
        return Icons.error_outline;
    }
  }

  ImageProvider getImage()
  {
    if(imagePath != null && imagePath != "")
      return new Image.file(new File(imagePath), fit: BoxFit.cover).image;

    else return new AssetImage("graphics/activities.png");
  }

  static DailyChat newFromJSON(Map json)
  {
    return new DailyChat
    (
      title: json["title"],
      moodId: json["moodId"],
      dateDayName: json["date"].toString().split(",")[0],
      dateDayOfMonth: json["date"].toString().split(" ")[1],
      dateMonth: json["date"].toString().split(" ")[2],
      dateYear: int.parse(json["date"].toString().split(" ")[3].trim()),
      imagePath: json["imagePath"],
      imageTurns : json["imageTurns"],
      chats: json["chats"],
      chatsPos: json["chatsPos"]
    );
  }
}