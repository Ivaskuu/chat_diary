import 'package:flutter/material.dart';

class ImageView extends StatefulWidget
{
  Image image;
  String title;
  String text;
  int turns;
  
  ImageView(this.image, this.turns, {this.title, this.text});

  @override
  State createState() => new ImageViewState(image, turns, title: title, text: text);
}

class ImageViewState extends State<ImageView>
{
  Image image;
  String title;
  String text;
  int turns;

  ImageViewState(this.image, this.turns, {this.title, this.text});

  bool showText = true;

  @override
  Widget build(BuildContext context)
  {
    return new Scaffold
    (
      appBar: new AppBar
      (
        leading: new IconButton(icon: new Icon(Icons.arrow_back), color: Colors.white, onPressed: () => Navigator.of(context).pop()),
        backgroundColor: Colors.black12,
        elevation: 0.0,
        title: title != null ? new Text(title) : null
      ),
      backgroundColor: Colors.black,
      body: new GestureDetector
      (
        onTap: () => setState(() => showText = !showText),
        onVerticalDragStart: (_) => Navigator.of(context).pop(),
        child: text != null && text.trim() != "" && showText ? imageAndText(context) : new Center(child: new RotatedBox(quarterTurns: turns, child: image))
      )
    );
  }

  Widget imageAndText(BuildContext context)
  {
    return new Stack
    (
      alignment: FractionalOffset.center,
      children: <Widget>
      [
        new RotatedBox(quarterTurns: turns, child: image),
        new Column
        (
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>
          [
            new Container
            (
              color: Colors.white10,
              child: new Container(margin: new EdgeInsets.all(16.0), child: new Text(text, textAlign: TextAlign.center, style: Theme.of(context).textTheme.subhead.copyWith(color: Colors.white)))
            )
          ]
        )
      ]
    );
  }
}