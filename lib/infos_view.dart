import 'package:flutter/material.dart';

class InfosView extends StatelessWidget
{
  PageController controller = new PageController();
  BuildContext cntxt;

  @override
  Widget build(BuildContext context)
  {
    cntxt = context;

    return new Scaffold
    (
      appBar: new AppBar
      (
        backgroundColor: Colors.white,
        title: new Text("Infos", style: new TextStyle(color: Colors.black)),
        leading: new IconButton(icon: new Icon(Icons.arrow_back), color: Colors.black, onPressed: () => Navigator.of(context).pop())
      ),
      body: new PageView
      (
        controller: controller,
        scrollDirection: Axis.vertical,
        children: <Widget>
        [
          new Column
          (
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>
            [
              new Expanded
              (
                child: new Container
                (
                  margin: new EdgeInsets.all(64.0),
                  decoration: new BoxDecoration
                  (
                    shape: BoxShape.circle,
                    image: new DecorationImage
                    (
                      image: new AssetImage("graphics/me.jpg"),
                      fit: BoxFit.cover
                    )
                  )
                )
              ),
              new Container
              (
                margin: new EdgeInsets.all(16.0),
                child: new Text("Ivascu Adrian", style: Theme.of(context).textTheme.title)
              ),
              new Container
              (
                margin: new EdgeInsets.all(48.0),
                child: new Text("This is me, who made this app. I am 17 years old, but I've started programming since I was 12 years old.", textAlign: TextAlign.center)
              )
            ]
          ),
          new Column
          (
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>
            [
              new Expanded
              (
                child: new Container
                (
                  margin: new EdgeInsets.all(64.0),
                  decoration: new BoxDecoration
                  (
                    shape: BoxShape.circle,
                    image: new DecorationImage
                    (
                      image: new AssetImage("graphics/skuu_labs.png"),
                      fit: BoxFit.cover
                    )
                  )
                )
              ),
              new Container
              (
                margin: new EdgeInsets.all(16.0),
                child: new Text("Skuu Labs", style: Theme.of(context).textTheme.title)
              ),
              new Container
              (
                margin: new EdgeInsets.all(32.0),
                child: new Text("Skuu Labs is an App / Games developers group, composed only of me (Ivascu Adrian) for the moment. The name was inspired by my name (Ivascu -> Ivaskuu -> skuu). I know, I am not very creative.", textAlign: TextAlign.center)
              )
            ]
          ),
          new Column
          (
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>
            [
              new Expanded
              (
                child: new Container
                (
                  margin: new EdgeInsets.all(64.0),
                  decoration: new BoxDecoration
                  (
                    shape: BoxShape.circle,
                    image: new DecorationImage
                    (
                      image: new AssetImage("graphics/logo.png"),
                      fit: BoxFit.cover
                    )
                  )
                )
              ),
              new Container
              (
                margin: new EdgeInsets.all(16.0),
                child: new Text("stry. beta 0.0.8", style: Theme.of(context).textTheme.title)
              ),
              new Container
              (
                margin: new EdgeInsets.all(32.0),
                child: new Text("stry. is an app we / I always wanted. It is very hard and time consuming to keep a physical diary, and having a simple text editing-form diary is still time consuming and after some time it becomes only a big mess. People needed something different. That's why stry. was born.", textAlign: TextAlign.center)
              )
            ]
          ),
          new Column
          (
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>
            [
              new Container(margin: new EdgeInsets.all(32.0), child: new Text("stry. is still in beta so there may still be some bugs and features to be implemented. Thank you for using this app and for your patience.", textAlign: TextAlign.center)),
              //new Container(margin: new EdgeInsets.only(top: 64.0), child: new RaisedButton(child: new Text("PRIVACY POLICY"), onPressed: () => showPrivacyPolicy())),
              new Container(margin: new EdgeInsets.only(top: 64.0), child: new RaisedButton(child: new Text("ABOUT"), onPressed: () => showDialog(context: cntxt, child: new AboutDialog()))),
            ]
          )
        ]
      )
    );
  }
  
  showPrivacyPolicy()
  {
    showDialog
    (
      context: cntxt,
      child: new AlertDialog
      (
        content: new ListView
        (
          children: <Widget>
          [
            new Text
            (
              '''
              This is the Privacy Policy.
              We respect your privacy and we don't collect private data. We only collect informations to make stry. better :
                > The time you open and close the app
                > The number of characters per DailyChat (only the number of characters, not their content)
              
              We reserve the right to change the Privacy Policy at any time with or without informing you, so the user of this app should always check the Privacy Policy.
              The user that don't agrees (i.e "disagrees") with this Privacy Policy MUST NOT use the app.
              '''
            )
          ]
        ),
        actions: <Widget>
        [
          new FlatButton(child: new Text("I AGREE"), onPressed: () => Navigator.of(cntxt).pop())
        ]
      )
    );
  }
  
  showTheOtherThing()
  {
    showDialog
    (
      context: cntxt,
      child: new AlertDialog
      (
        content: new ListView
        (
          children: <Widget>
          [
            new Text
            (
              '''
              This is the Privacy Policy.
              We respect your privacy and we don't collect private data. We only collect informations to make stry. better :
                > The time you open and close the app
                > The number of characters per DailyChat (only the number of characters, not their content)
              
              We reserve the right to change the Privacy Policy at any time with or without informing you, so the user of this app should always check the Privacy Policy.
              The user that don't agrees (i.e "disagrees") with this Privacy Policy MUST NOT use the app.
              '''
            )
          ]
        ),
        actions: <Widget>
        [
          new FlatButton(child: new Text("I AGREE"), onPressed: () => Navigator.of(cntxt).pop())
        ]
      )
    );
  }
}