import 'package:flutter/material.dart';
import 'byrefstring.dart';

class NewImage extends StatefulWidget
{
  Image image;
  ByRefString turns;
  ByRefString text;
  NewImage(this.image, this.turns, {this.text});

  @override
  State createState() => new NewImageState(image, turns, text);
}

class NewImageState extends State<NewImage>
{
  Image image;
  ByRefString turns;
  ByRefString text;
  NewImageState(this.image, this.turns, this.text);

  int rotation = 0;
  TextEditingController controller = new TextEditingController();

  @override
  Widget build(BuildContext context)
  {
    turns.string = rotation.toString();

    return new Scaffold
    (
      appBar: new AppBar
      (
        leading: new IconButton(icon: new Icon(Icons.arrow_back), color: Colors.white, onPressed: () { turns.string = "null"; Navigator.of(context).pop(); }),
        backgroundColor: Colors.white10,
        title: new Text("Rotate the image"),
        actions: <Widget>
        [
          new IconButton(icon: new Icon(Icons.rotate_right), onPressed: () => setState(() => rotation < 3 ? rotation++ : rotation = 0)),
          new IconButton(icon: new Icon(Icons.done), onPressed: () => Navigator.of(context).pop())
        ]
      ),
      backgroundColor: Colors.black,
      body: text == null
        ? new Center(child: new RotatedBox(quarterTurns: rotation, child: image))
        : imageAndTextField()
    );
  }

  Widget imageAndTextField()
  {
    return new Stack
    (
      alignment: FractionalOffset.center,
      children: <Widget>
      [
        new RotatedBox(quarterTurns: rotation, child: image),
        new Column
        (
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>
          [
            new Container
            (
              color: Colors.white10,
              child: new Container
              (
                margin: new EdgeInsets.symmetric(horizontal: 12.0),
                child: new TextField
                (
                  controller: controller,
                  autofocus: true,
                  keyboardType: TextInputType.text,
                  onChanged: (value) => text.string = value,
                  style: Theme.of(context).textTheme.title.copyWith(color: Colors.white)
                )
              )
            )
          ]
        )
      ]
    );
  }
}