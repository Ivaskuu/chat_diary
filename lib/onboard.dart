import 'package:flutter/material.dart';
import 'main.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'dart:math';

class Onboard extends StatefulWidget
{
  @override
  State createState() => new OnboardState();
}

class OnboardState extends State<Onboard>
{
  PageController controller = new PageController();

  @override
  void initState()
  {
    super.initState();
  }

  @override
  Widget build(BuildContext context)
  {
    return new Scaffold
    (
      body: new PageView
      (
        controller: controller,
        scrollDirection: Axis.vertical,
        children: <Widget>
        [
          // Animation of someone writing a chat message, and the other person responding
          onboardPage("Why would diary apps suck ?", "Daily Chat let you describe and analyze your days in the most natural way: by chatting!"),
          // Animation of the sentiment icon morphing from very unsatisfied to very satisfied
          onboardPage("The most innovative diary app", "Use the mood indicator and weekly overview to analyze how you are spending your days and fix your bad habits."),
          // Animation of clicking on a button then a triangle with an ! appears
          onboardPage("Daily Chat is still in beta", "This means that there are stil improvements to be made and bugs to be corrected. We're working hard day and night to make Daily Chat better."),
          enterNamePage(),
          finalOnboardPage("Welcome aboard ${nameController.text}.", "You are now ready to explore your new diary app :-)")          
        ]
      )
    );
  }

  Widget onboardPage(String title, String desc)
  {
    return new Container
    (
      margin: new EdgeInsets.all(32.0),
      child: new Column
      (
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>
        [
          new Container(margin: new EdgeInsets.only(bottom: 48.0), child: new CircleAvatar(radius: 75.0)),
          new Container(margin: new EdgeInsets.only(bottom: 14.0), child: new Text(title, textAlign: TextAlign.center, style: Theme.of(context).textTheme.title)),
          new Text(desc, textAlign: TextAlign.center, textScaleFactor: 1.3, style: Theme.of(context).textTheme.caption),
          new Container
          (
            margin: new EdgeInsets.only(top: 72.0),
            child: new FloatingActionButton
            (
              backgroundColor: Colors.black,
              child: new Icon(Icons.arrow_downward),
              onPressed: ()
              {
                controller.nextPage(duration: new Duration(milliseconds: 400), curve: Curves.easeInOut);
              }
            )
          )
        ]
      )
    );
  }

  TextEditingController nameController = new TextEditingController();

  Widget enterNamePage()
  {
    return new ListView
    (
      padding: new EdgeInsets.only(top: 64.0),
      children: <Widget>
      [
        new Container
        (
          margin: new EdgeInsets.all(32.0),
          child: new Column
          (
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>
            [
              new Container(margin: new EdgeInsets.only(bottom: 48.0), child: new Icon(Icons.account_circle, size: 150.0)),
              new Container(margin: new EdgeInsets.only(bottom: 14.0), child: new Text("What is your name ?", textAlign: TextAlign.center, style: Theme.of(context).textTheme.title)),
              new Container(margin: new EdgeInsets.only(bottom: 14.0, left: 58.0, right: 58.0), child: new TextField(textAlign: TextAlign.center, controller: nameController, decoration: new InputDecoration(hintText: "Your name"), onChanged: (_) => setState((){}))),
              !nameController.text.contains(" ") && nameController.text != ""
              ? new Container
              (
                margin: new EdgeInsets.only(top: 72.0),
                child: new FloatingActionButton
                (
                  backgroundColor: Colors.black,
                  elevation: 5.0,
                  child: new Icon(Icons.arrow_downward),
                  onPressed: ()
                  {
                    saveNameToPrefs();
                    controller.nextPage(duration: new Duration(milliseconds: 400), curve: Curves.easeInOut);
                  }
                )
              ) : new Container()
            ]
          )
        )
      ]
    );
  }

  Widget finalOnboardPage(String title, String desc)
  {
    return new Container
    (
      margin: new EdgeInsets.all(32.0),
      child: new Column
      (
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>
        [
          new Container(margin: new EdgeInsets.only(bottom: 32.0), child: new Text(title, textScaleFactor: 1.8, textAlign: TextAlign.center, style: Theme.of(context).textTheme.title)),
          new Text(desc, textAlign: TextAlign.center, textScaleFactor: 1.3, style: Theme.of(context).textTheme.caption),
          new Container
          (
            margin: new EdgeInsets.only(top: 96.0),
            child: new Row
            (
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children:
              [
                new FadingIcon(),
                new FadingIcon(),
                new FadingIcon()
              ]
            )
          ),
          new Container(margin: new EdgeInsets.only(top: 96.0), child: new RaisedButton(color: Colors.black, child: new Text("START MY JOURNEY", style: new TextStyle(color: Colors.white)), onPressed: () => Navigator.of(context).pushReplacement(new MaterialPageRoute(builder: (_) => new MainLayout()))))
        ]
      )
    );
  }

  saveNameToPrefs() async
  {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("userName", nameController.text.trim());
    await prefs.commit();

    return true;
  }
}

class FadingIcon extends StatefulWidget
{
  FadingIconState child;

  @override
  State createState()
  {
    child = new FadingIconState();
    return child;
  }
}

class FadingIconState extends State<FadingIcon> with SingleTickerProviderStateMixin
{
  AnimationController animController;
  IconData icon = Icons.error;
  int lastIcon;

  @override
  void initState()
  {
    Random rand = new Random();

    animController = new AnimationController(duration: new Duration(milliseconds: 1000 + rand.nextInt(1000)), vsync: this);
    animController.forward();
    animController.addStatusListener((AnimationStatus status) => changeAnim(status));

    icon = getRandomIcon();

    super.initState();
  }

  @override
  Widget build(BuildContext context)
  {
    return new FadeTransition
    (
      opacity: new CurvedAnimation(parent: animController, curve: Curves.ease),
      child: new Icon(icon, size: 60.0),
    );
  }

  void changeAnim(AnimationStatus status)
  {
    if(status == AnimationStatus.completed)
    {
      animController.reverse();
    }
    else if(status == AnimationStatus.dismissed)
    {
      setState(() => icon = getRandomIcon());
      animController.forward();
    }
  }

  IconData getRandomIcon()
  {
    Random rand = new Random();
    int newIcon;

    // Don't have the same icon 2 times in a row
    do
    {
      newIcon = rand.nextInt(27);
    } while(lastIcon == newIcon);
    lastIcon = newIcon;

    switch(newIcon)
    {
      case 0: return Icons.account_balance;
      case 1: return Icons.shopping_cart;
      case 2: return Icons.face;
      case 3: return Icons.favorite;
      case 4: return Icons.motorcycle;
      case 5: return Icons.rowing;
      case 6: return Icons.thumbs_up_down;
      case 7: return Icons.theaters;
      case 8: return Icons.weekend;
      case 9: return Icons.gamepad;
      case 10: return Icons.laptop;
      case 11: return Icons.image;
      case 12: return Icons.landscape;
      case 13: return Icons.nature;
      case 14: return Icons.remove_red_eye;
      case 15: return Icons.tag_faces;
      case 16: return Icons.directions_bike;
      case 17: return Icons.directions_walk;
      case 18: return Icons.directions_run;
      case 19: return Icons.flight;
      case 20: return Icons.hot_tub;
      case 21: return Icons.pool;
      case 22: return Icons.golf_course;
      case 23: return Icons.fitness_center;
      case 24: return Icons.cake;
      case 25: return Icons.public;
      case 26: return Icons.book;
      default: return Icons.error;
    }
  }
}