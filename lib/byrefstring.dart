// Needed to communicate between Navigator views
// String is not passed by reference but this class is

class ByRefString
{
  String string;
  ByRefString(this.string);
}