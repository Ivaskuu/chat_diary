import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InsertPinView extends StatelessWidget
{
  List<int> taps = new List<int>();
  List<int> code = new List<int>();
  
  BuildContext context;

  @override
  Widget build(BuildContext ctxt)
  {
    context = ctxt;
    loadPrefs();

    return new Scaffold
    (
      body: new Container
      (
        margin: new EdgeInsets.all(10.0),
        child: new Column
        (
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>
          [
            new Container(margin: new EdgeInsets.symmetric(vertical: 64.0, horizontal: 8.0), child: new Text("Please enter your code", textAlign: TextAlign.center)),
            new Expanded(child:new GridView.count
            (
              crossAxisCount: 2,
              children: <Widget>
              [
                new InkWell(onTap: () { taps.add(0); verifyTaps(); }),
                new InkWell(onTap: () { taps.add(1); verifyTaps(); }),
                new InkWell(onTap: () { taps.add(2); verifyTaps(); }),
                new InkWell(onTap: () { taps.add(3); verifyTaps(); })
              ]
            ))
          ]
        )
      )
    );
  }

  loadPrefs() async
  {
    var prefs = await SharedPreferences.getInstance();
    List<String> stringCode = prefs.getStringList("pinCode");

    for(int i = 0; i < stringCode.length; i++)
    {
      code.add(int.parse(stringCode[i]));
    }
  }

  void verifyTaps()
  {
    if(taps.length >= code.length)
    {
      for(int i = 0; i < code.length; i++)
      {
        if(code[code.length - 1 - i] != taps[taps.length - 1 - i])
        {
          return;
        }
      }

      Navigator.of(context).pop(true);
    }
  }
}