import 'package:flutter/material.dart';
import 'dailychat.dart';
import 'dailychat_edit_chat.dart';
import 'main.dart';

class DailyChatChatFullscreen extends StatefulWidget
{
  DailyChat dailyChat;
  CustomIO io;
  DailyChatChatFullscreen(this.dailyChat, this.io);

  @override
  State createState() => new DailyChatChatFullscreenState(dailyChat, io);
}

class DailyChatChatFullscreenState extends State<DailyChatChatFullscreen>
{
  DailyChat dailyChat;
  CustomIO io;
  DailyChatChatFullscreenState(this.dailyChat, this.io);

  @override
  Widget build(BuildContext context)
  {
    return new Scaffold
    (
      appBar: new AppBar
      (
        elevation: 2.0,
        backgroundColor: Colors.white,
        leading: new IconButton(icon: new Icon(Icons.close, color: Colors.black), onPressed: () => Navigator.of(context).pop()),
        title: new Text(dailyChat.getCompleteDate(false), style: new TextStyle(color: Colors.black))
      ),
      body: new DailyChatEditChat(dailyChat, true, io)
    );
  }
}