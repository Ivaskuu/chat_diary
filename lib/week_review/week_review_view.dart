/// A view where are listed the weeks created automatically based
/// on the user DailyChats

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'week_view.dart';
import '../dailychat.dart';

class WeekReviewView extends StatefulWidget
{
  List<List<DailyChat>> weeks;
  WeekReviewView(this.weeks);
  
  @override
  State createState() => new WeekReviewViewState(weeks);
}

class WeekReviewViewState extends State<WeekReviewView>
{
  List<List<DailyChat>> weeks;
  WeekReviewViewState(this.weeks);
  
  List<WeekCard> weekCards;
  
  @override
  void initState()
  {
    print(weeks.length);

    weekCards = new List<WeekCard>();
    for(int i = 0; i < weeks.length; i++)
    {
      weekCards.insert(0, new WeekCard(weeks[i]));
    }
    
    super.initState();
  }
  
  @override
  Widget build(BuildContext context)
  {
    return new Scaffold
    (
      appBar: new AppBar
      (
        backgroundColor: Colors.white,
        leading: new IconButton(color: Colors.black, icon: new Icon(Icons.arrow_back), onPressed: () => Navigator.of(context).pop()),
        title: new Text("Week Review", style: new TextStyle(color: Colors.black)),
        elevation: 2.0
      ),
      body: new ListView
      (
        children: weekCards
      )
    );
  }
}

class WeekCard extends StatelessWidget
{
  List<DailyChat> dailyChats;
  WeekCard(this.dailyChats);
  
  @override
  Widget build(BuildContext context)
  {
    return new Container
    (
      margin: new EdgeInsets.all(8.0),
      child: new Stack
      (
        alignment: new FractionalOffset(0.03, 2.0),
        children: <Widget>
        [
          new Card
          (
            child: new InkWell
            (
              onTap: () => Navigator.of(context).push(new MaterialPageRoute(builder: (_) => new WeekView(dailyChats, getStartDate(), getFinnishDate()))),
              child: new Row
              (
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>
                [
                  new SizedBox.fromSize
                  (
                    size: new Size(130.0, 140.0),
                    child: new Container()
                  ),
                  new Expanded
                  (
                    child: new Container
                    (
                      margin: new EdgeInsets.all(16.0),
                      child: new Column
                      (
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>
                        [
                          new Text("Week title", style: Theme.of(context).textTheme.title),
                          //new Text("Some messages from the week", style: Theme.of(context).textTheme.caption),
                          new Container
                          (
                            margin: new EdgeInsets.only(top: 4.0, bottom: 24.0),
                            child: new Text("From ${getStartDate()} to ${getFinnishDate()}", style: Theme.of(context).textTheme.caption)
                          ),
                          new Icon(Icons.sentiment_satisfied)
                        ]
                      )
                    )
                  )
                ]
              )
            )
          ),
          new Card
          (
            child: new SizedBox.fromSize
            (
              size: new Size(120.0, 150.0),
              child: new RotatedBox
              (
                quarterTurns: 0,//dailyChat[0].imageTurns,
                child: new Container
                (
                  child: new Image.asset("graphics/activities_zoom.png", fit: BoxFit.cover)
                  /*
                  decoration: new BoxDecoration
                  (
                    image: new DecorationImage
                    (
                      image: new Image.asset("graphics/material_image.jpg", fit: BoxFit.cover),
                      fit: BoxFit.cover
                    )
                  )*/
                )
              )
            )
          )
        ]
      )
    );
  }
  
  String getStartDate()
  {
    for(int i = 0; i < dailyChats.length; i++)
    {
      if(dailyChats[i] != null)
      {
        DateFormat dateCheck = new DateFormat("EEEE, d MMMM yyyy");
        DateTime dcDateTime = dateCheck.parse(dailyChats[i].getCompleteDate(true));
        
        dcDateTime = dcDateTime.subtract(new Duration(days: i));
        return "${dcDateTime.day}/${dcDateTime.month}";
      }
    }
  }
  
  String getFinnishDate()
  {
    for(int i = dailyChats.length - 1; i > 0; i--)
    {
      if(dailyChats[i] != null)
      {
        DateFormat dateCheck = new DateFormat("EEEE, d MMMM yyyy");
        DateTime dcDateTime = dateCheck.parse(dailyChats[i].getCompleteDate(true));
        
        dcDateTime = dcDateTime.add(new Duration(days: i));
        return "${dcDateTime.day}/${dcDateTime.month}";
      }
    }
  }

  // TODO
  Widget imagesCollage()
  {
    return new Row
    (
      children: <Widget>
      [
        new Expanded(flex: 1, child: new Image.asset("graphics/park.jpg", fit: BoxFit.scaleDown)),
        new Expanded(flex: 2, child: new Column
        (
          children: <Widget>
          [
            new Image.asset("graphics/park.jpg", fit: BoxFit.scaleDown),
          ]
        ))
      ]
    );
  }
}