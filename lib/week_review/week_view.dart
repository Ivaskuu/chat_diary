import 'package:flutter/material.dart';
import '../dailychat.dart';

class WeekView extends StatefulWidget
{
  List<DailyChat> dailyChats;
  String startDate;
  String finnishDate;
  WeekView(this.dailyChats, this.startDate, this.finnishDate);

  @override
  State createState() => new WeekViewState(dailyChats, startDate, finnishDate);
}

class WeekViewState extends State<WeekView>
{
  List<DailyChat> dailyChats;
  String startDate;
  String finnishDate;
  WeekViewState(this.dailyChats, this.startDate, this.finnishDate);
  
  List weekDays = new List();

  @override
  void initState()
  {
    for(int i = 0; i < dailyChats.length; i++)
    {
      if(dailyChats[i] == null) weekDays.add(noweekday());
      else weekDays.add(weekday(i, dailyChats[i]));
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context)
  {
    return new Scaffold
    (
      appBar: new PreferredSize
      (
        preferredSize: new Size.fromHeight(200.0),
        child: new Stack
        (
          alignment: FractionalOffset.center,
          fit: StackFit.expand,
          children: <Widget>
          [
            new AppBar
            (
              backgroundColor: Colors.transparent,
              leading: new IconButton(color: Colors.black, icon: new Icon(Icons.close), onPressed: () => Navigator.of(context).pop()),
              elevation: 0.0
            ),
            new Column
            (
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>
              [
                new Container(margin: new EdgeInsets.only(bottom: 20.0), child: new CircleAvatar(radius: 50.0, backgroundColor: Colors.teal)),
                new Text("Una settimana al mare", style: Theme.of(context).textTheme.title),
                new Container(margin: new EdgeInsets.only(bottom: 20.0), child: new Text("Dal $startDate al $finnishDate", style: Theme.of(context).textTheme.caption, textScaleFactor: 1.3))
              ]
            )
          ]
        )
      ),
      body: new ListView
      (
        children: weekDays
      )
    );
  }

  // A list card with the day of the DailyChat and some messages of that day
  Widget weekday(int day, DailyChat dc)
  {
    String dayWeek;
    switch(day) { case 0: dayWeek = "L"; break; case 1: dayWeek = "M"; break; case 2: dayWeek = "M"; break; case 3: dayWeek = "G"; break;
    case 4: dayWeek = "V"; break; case 5: dayWeek = "S"; break; default: dayWeek = "D"; break; }

    return new Stack
    (
      children: <Widget>
      [
        new Card
        (
          child: new InkWell
          (
            child: new ListTile
            (
              leading: new CircleAvatar
              (
                backgroundColor: Colors.black12,
                child: new Stack
                (
                  alignment: new FractionalOffset(0.5, 0.5),
                  children:
                  [
                    new RotatedBox
                    (
                      quarterTurns: dc.imageTurns,
                      child: new Container
                      (
                        decoration: new BoxDecoration
                        (
                          shape: BoxShape.circle,
                          image: new DecorationImage
                          (
                            image: dc.getImage(),
                            fit: BoxFit.cover
                          )
                        )
                      )
                    ),
                    new Text(dayWeek)
                  ]
                )
              ),
              title: new Text(dc.title),
              subtitle: new Text("Work in progress")
            ),
            onTap: () {},
          )
        ),
        /*new Align
        (
          alignment: FractionalOffset.centerRight,
          child: new Image.asset("graphics/park.jpg", fit: BoxFit.contain)
        )*/
      ]
    );
  }

  // When there isn't a DailyChat for this day
  Widget noweekday()
  {
    return new Card
    (
      child: new ListTile
      (
        subtitle: new Text("Nothing to show for this day.")
      ),
    );
  }
}